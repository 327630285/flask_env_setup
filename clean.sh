#! /bin/bash

cd ./env

rm -rf nginx-1.10.3 pcre-8.40  Python-3.5.3

cd ..

cd ./packages

rm -rf click-6.6 Flask-Script-2.0.5 itsdangerous-0.24 MarkupSafe-0.23 uwsgi-2.0.14 Werkzeug-0.11.10 Flask-0.12 Flask-SQLAlchemy-2.1 Jinja2-2.8 SQLAlchemy-1.1.0b3 virtualenv-15.1.0 cx_Oracle-5.2.1

rm -rf PyInstaller-3.2.1 

cd ..
