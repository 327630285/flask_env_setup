#!/bin/bash
fdisk /dev/xvdb << EOF
n
p
1


wq
EOF


mkfs.ext3 /dev/xvdb1
if [ -e /www ]
then
exit;
fi
mkdir /www

echo '/dev/xvdb1             /www                 ext3    defaults        1 2' >> /etc/fstab
mount -a

