#!/bin/bash

Add_Startup()
{
    echo "Add Startup and Starting WebApp..."
    \cp init.d/nginx /etc/init.d/nginx
    \cp conf/webapp /usr/bin/webapp
    chmod +x /usr/bin/webapp
    StartUp nginx
    /etc/init.d/nginx start
}

Add_Startup