#! /bin/bash

cd ./packages

#tar zxvf virtualenv-15.1.0.tar.gz
#cd virtualenv-15.1.0
#python setup.py install
#cd ..

#tar zxvf uwsgi-2.0.14.tar.gz
#cd uwsgi-2.0.14
#python setup.py install
#cd ..

pkg_list=('virtualenv-15.1.0' 'uwsgi-2.0.14' 'click-6.6' 'itsdangerous-0.24' 'MarkupSafe-0.23' 'cx_Oracle-5.2.1' 'Werkzeug-0.11.10' 'Jinja2-2.8' 'Flask-0.12' 'Flask-Script-2.0.5' 'SQLAlchemy-1.1.0b3' 'Flask-SQLAlchemy-2.1')

for pkg_name in ${pkg_list[@]}
do
	tar zxvf $pkg_name'.tar.gz'
	cd $pkg_name
	python setup.py install
	cd ..
done

cd ..

cp -fR ./conf/config.ini /www/python/host/config.ini

if [ -h "/usr/local/bin/uwsgi" ]; then
	rm -rf /usr/local/bin/uwsgi
fi

ln -s /usr/local/python3/bin/uwsgi /usr/local/bin/uwsgi

