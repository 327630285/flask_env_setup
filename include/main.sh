#!/bin/bash

Color_Text()
{
  echo -e " \e[0;$2m$1\e[0m"
}

Echo_Green()
{
  echo $(Color_Text "$1" "32")
}