#! /bin/bash

cd ./env

tar zxvf pcre-8.40.tar.gz
tar zxvf nginx-1.10.3.tar.gz

cd pcre-8.40
./configure --prefix=/usr/local/pcre
make
make install
cd ..

grep -wq "export PATH=/usr/local/pcre/bin:\$PATH" /etc/bash.bashrc || echo 'export PATH=/usr/local/pcre/bin:$PATH' >> /etc/bash.bashrc
grep -wq "export LD_LIBRARY_PATH=\${LD_LIBRARY_PATH}:/usr/local/pcre/lib" /etc/bash.bashrc || echo 'export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/usr/local/pcre/lib' >> /etc/bash.bashrc

source /etc/bash.bashrc

cd nginx-1.10.3
./configure --user=http --group=http --prefix=/usr/local/nginx --with-pcre=../pcre-8.40
make
make install
chmod 775 /usr/local/nginx/logs
chown -R http:http /usr/local/nginx/logs
chmod -R 775 /www
chown -R http:http /www
cd ../.. # back to flask install root dir
cp -fR ./conf/nginx.conf /usr/local/nginx/conf/nginx.conf
chmod -R 755 /usr/local/nginx/sbin/nginx
/usr/local/nginx/sbin/nginx
