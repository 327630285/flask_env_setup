#! /bin/bash

cd ./env/oracle_client
unzip -o instantclient-basic-linux.x64-12.1.0.2.0.zip
unzip -o instantclient-sdk-linux.x64-12.1.0.2.0.zip


mv ./instantclient_12_1 /opt/instantclient

ln -s /opt/instantclient/libclntsh.so.12.1 /opt/instantclient/libclntsh.so

#export ORACLE_HOME=/opt/instantclient
#export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$ORACLE_HOME

grep -wq "export ORACLE_HOME=/opt/instantclient" /etc/bash.bashrc || echo 'export ORACLE_HOME=/opt/instantclient' >> /etc/bash.bashrc
#grep -wq "export LD_LIBRARY_PATH=\${LD_LIBRARY_PATH}:\$ORACLE_HOME" /etc/bash.bashrc || echo 'export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:$ORACLE_HOME' >> /etc/bash.bashrc
source /etc/bash.bashrc

cd ../..
