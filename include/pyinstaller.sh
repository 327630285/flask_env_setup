#! /bin/bash

cd ./packages

if [ -d "PyInstaller-3.2.1" ]; then
	rm -rf PyInstaller-3.2.1
fi


tar jxvf 'PyInstaller-3.2.1.tar.bz2'
cd PyInstaller-3.2.1
python setup.py install
cd ..

cd ..