#! /bin/bash
cd ./env

if [ -d "Python-3.5.3" ]; then
	rm -rf ./Python-3.5.3
fi

if [ -d "/usr/local/python3" ]; then
	rm -rf /usr/local/python3
fi

if [ -h "/usr/bin/python3" ]; then
	rm -rf /usr/bin/python3
fi

tar zxvf Python-3.5.3.tgz
cd ./Python-3.5.3
./configure  --enable-shared --prefix=/usr/local/python3
make
make install
cd ..

cd ..

ln -s /usr/local/python3/bin/python3.5 /usr/bin/python3
if [ -h "/usr/bin/python" ]; then
	rm -rf /usr/bin/python
fi
ln -s /usr/bin/python3 /usr/bin/python
