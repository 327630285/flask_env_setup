#! /bin/bash

export python_version=3.5.3
export nginx_version=1.10.3
export pcre_version=8.40

touch tmp.log

env_app=0

. include/main.sh

read -p "full install Flask envirment, input yes or no : " is_full_install

if [ $is_full_install == 'yes' ];then
	echo "system will install nginx + python + Flask + uWSGI"
else
	Echo_Green "You have 5 options for your install."
    echo "1: Install nginx"
    echo "2: Install Python"
    echo "3: Install Oracle client"
    echo "4: Install Flask and the related packages"
    echo "5: Setup envionment variables and command line"
	read -p "Enter your choice (1, 2, 3, 4 or 5) : " env_app
fi


if [ $env_app == 1 ] || [ $is_full_install == 'yes' ];then
	if [ -e /dev/xvdb ]
		then
		./include/disk.sh
		echo "---------- add disk ok ----------" >> tmp.log
		else
		mkdir /www
	fi
	./include/env.sh
	echo "---------- env ok ----------" >> tmp.log
	./include/nginx_1.10.3.sh
	echo "---------- nginx ok ----------" >> tmp.log
fi

if [ $env_app == 2 ] || [ $is_full_install == 'yes' ];then
./include/python_3.5.3.sh
echo "---------- python ok ----------" >> tmp.log
fi

if [ $env_app == 3 ] || [ $is_full_install == 'yes' ];then
./include/oracle_client.sh
echo "---------- oracle client ok ----------" >> tmp.log
fi

if [ $env_app == 4 ] || [ $is_full_install == 'yes' ];then
./include/flask.sh
echo "---------- flask ok ----------" >> tmp.log
fi

if [ $env_app == 5 ] || [ $is_full_install == 'yes' ];then
./include/end.sh
echo "---------- install end ----------" >> tmp.log
fi

grep -wq "export LD_LIBRARY_PATH=/opt/instantclient:/usr/local/pcre/lib:/usr/local/python3/lib" /etc/bash.bashrc || echo 'export LD_LIBRARY_PATH=/opt/instantclient:/usr/local/pcre/lib:/usr/local/python3/lib' >> /etc/bash.bashrc
source /etc/bash.bashrc